export const USER_INIT = "USER_INIT";
export const FIELD_ADD = "FIELD_ADD";
export const FIELD_DELETE = "FIELD_DELETE";
export const FIELD_CHANGEPOSITION = "FIELD_CHANGEPOSITION";
export const GROUP_INIT = "GROUP_INIT";
export const GROUP_ADD = "GROUP_ADD";
export const GROUP_DELETE = "GROUP_DELETE";
export const GROUP_CHANGEPOSITION = "GROUP_CHANGEPOSITION";


export function initUser(data) {
	return { type: USER_INIT, data }
}


export function initGroup(data) {
	return { type: GROUP_INIT, data }
}
export function addGroup(name) {
	return { type: GROUP_ADD, name };
}
export function deleteGroup(groupId) {
	return { type: GROUP_DELETE, groupId };
}
export function changeGroupPosition(groupId, position) {
	return { type: GROUP_CHANGEPOSITION, groupId, position };
}



export function addField(groupId, name, type, value) {
	return { type: FIELD_ADD, groupId, name, typeInput:type, value };
}
export function deleteField(groupId,fieldId) {
	return { type: FIELD_DELETE, groupId, fieldId };
}
export function changeField(groupId,fieldId) {
	return { type: FIELD_CHANGEPOSITION, groupId, fieldId, position };
}





