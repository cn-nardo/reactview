export const dataUser = [
	{
		user: 'nardo',
		id: 0,
		password: 'nardo12345',
		first_name: 'Chu',
		last_name: 'Nhan',
		address: 'Linh Trung, Thủ Đức',
		birthday: '10/04/1994',
		skill: [
			'HTML/CSS/JS',
			'PHP',
			'Wordpress',
			'Laravel',
			'ReactJs',
			'Redux'
		],
		company: [
			{
				company_name: 'Cybridge Asia',
				location: 'Dương Bá Trạc, P1, Q8, TP HCM',
				position: 'Staff'
			}
		]
	}
]

export const dataFields = [
	{
		groupId:0,
		name:'Base Informations',
		position:0,
		list:[
			{
				fieldId: 0,
				name: 'name',
				type: 'text',
				value: 'Chu Cong Hoai Nhan'
			},
			{
				fieldId: 1,
				name: 'email',
				type: 'email',
				value: 'chuconghoainhan@gmail.com',
			}
		]
	},
	{
		groupId:1,
		name:'Company Informations',
		position:1,
		list:[
			{
				fieldId: 0,
				name: 'company_name',
				type: 'text',
				value: 'Cybridge Asia'
			},
			{
				fieldId: 1,
				name: 'location',
				type: 'text',
				value: '133 Duong Ba Trac, Phuong 1, Quan 8, TP Ho Chi Minh',
			}
		]
	}
];