import React, { Component } from "react";
import ReactDOM from "react-dom";

class BackgroundComponent extends React.Component {
	render() {
		return (
			<div className="backgroundComponent">
				This is background Component of CV
			</div>
		);
	}
}

export default BackgroundComponent;