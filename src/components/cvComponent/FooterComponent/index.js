import React, { Component } from "react";
import ReactDOM from "react-dom";

class FooterComponent extends React.Component {
	render() {
		return(
			<div className="footerComponent">
				This is footer Component of CV
			</div>
		);
	}
}

export default FooterComponent;