import React, { Component } from "react";
import ReactDOM from "react-dom";

class MaininfoComponent extends React.Component {
	render() {
		return(
			<div className="maininfoComponent">
				This is main info Component of CV
			</div>
		);
	}
}

export default MaininfoComponent;