import React, { Component } from "react";
import ReactDOM from "react-dom";

import BackgroundComponent from "./BackgroundComponent/index";
import MaininfoComponent from "./MaininfoComponent/index";
import FooterComponent from "./FooterComponent/index";
import * as ReactBootstrap from 'react-bootstrap'

class CvComponent extends React.Component {
	render(){
		return(
			<Grid className="mainCv ">
				<Row className="show-grid">
					<Col md={3}>
						<BackgroundComponent />
					</Col>
					<Col md={9}>
						<MaininfoComponent />
						<FooterComponent />
					</Col>
				</Row>
			</Grid>
		)
	}
}

export default CvComponent;