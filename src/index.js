import React, { Component } from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { createStore } from "redux";
import cvForm from "./reducers/cvForm";
import CvComponent from "./components/cvComponent/index";
import { addGroup, addField, initGroup, initUser } from "./actions/index";
import {
	dataUser,
	dataFields } from "./apitemplate/index.js";

const store = createStore(cvForm);
store.dispatch(initUser(dataUser));
store.dispatch(initGroup(dataFields));
store.dispatch(addGroup("group01"));
store.dispatch(addField(0, "field01", "text", "valuefield"));

console.log(store.getState());

class App extends React.Component {
	render() {
		return (
			<Provider store={store}>
				<CvComponent />
			</Provider>
		)
	}
}

const rootElement = document.getElementById("root");
ReactDOM.render(<App />, rootElement);