import { combineReducers } from "redux";
import { 
	USER_INIT, 
	GROUP_INIT, 
	GROUP_ADD, 
	GROUP_DELETE,
	GROUP_CHANGEPOSITION,
	FIELD_ADD,
	FIELD_DELETE,
	FIELD_CHANGEPOSITION } from "../actions/index.js";
import {
	dataUser,
	dataFields } from "../apitemplate/index.js";
import {
	getIncrementId_field,
	getIncrementId_group,
	getIncrementId_user,
	getPosition_group } from "./helper";

const incrementId_group = getIncrementId_group();
const incrementId_user = getIncrementId_user();

function user ( state = [], action ) {
	switch (action.type) {
		case USER_INIT:
			return action.data;
		default:
			return state;
	}
}


function groups ( state = [], action ) {
	switch (action.type) {
		case GROUP_INIT:
			return action.data;
		case GROUP_ADD:
			const newsId = incrementId_group + 1;
			return [
				...state,
				{
					groupId: newsId,
					name: action.name,
					position: getPosition_group() + 1,
					list: []
				}
			];
		case GROUP_DELETE:
			return 'deletegroup';
		case GROUP_CHANGEPOSITION:
			return 'changepositiongroup';
		case FIELD_ADD:
			// get imcrementID of field by API (waiting)
			return state.map((group) => {
				if(group.groupId === action.groupId) {
					return {
						...group,
						list: [
							...group.list,
							{
								filedId: getIncrementId_field(action.groupId),
								name: action.name,
								typeInput: action.typeInput,
								value: action.value
							}
						]
					}
				}else{
					return group;
				}
			});
		case FIELD_DELETE:
			return 'deletefield';
		case FIELD_CHANGEPOSITION:
			return 'changepositionfield';
		default:
			return state;
	}
}

const cvForm = combineReducers({
	user,
	groups
})

export default cvForm;
