import {
	dataUser,
	dataFields } from "../apitemplate/index.js";
// function call to API

export function getIncrementId_field (groupId) {
	// get Increment ID of field
	dataFields.map((group,index) => {
		if(group.groupId === groupId) {
			return group.list.length;
		}
	});
	return false;
}
export function getIncrementId_group () {
	return dataFields.length;
}
export function getPosition_group () {
	let newPosition = 0;
	dataFields.map((value,index) => {
		if (value.position >= newPosition) newPosition = value.position;
	});
	return newPosition;
}
export function getIncrementId_user () {
	return dataUser.length;
}